// 1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )


function findWebDevelopers(employeeDetails){
    if(Array.isArray(employeeDetails)){
        let webDevelopers = [];
        for(employee of employeeDetails){
            if(employee.job.includes("Web Developer")){
                webDevelopers.push(employee);
            }
        }
        return webDevelopers;
    }
    else{
        return [];
    }
}

module.exports = findWebDevelopers;