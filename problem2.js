// 2. Convert all the salary values into proper numbers instead of strings.


function convertSalaryToNumber(employeeDetails){
    if(Array.isArray(employeeDetails)){
        for(employee of employeeDetails){
            let employeeSalary = employee.salary;
            employeeSalary = employeeSalary.replace("$", "");
            employee.salary = Number(employeeSalary);
        }
        return employeeDetails;
    }
    else{
        return [];
    }
}

module.exports = convertSalaryToNumber;