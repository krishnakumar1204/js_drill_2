// 3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)
const convertSalaryToNumber = require("./problem2.js");

function modifySalary(employeeDetails){
    if(Array.isArray(employeeDetails)){
        employeeDetails = convertSalaryToNumber(employeeDetails);
        for(employee of employeeDetails){
            employee.corrected_salary = employee.salary*10000;
        }
        return employeeDetails;
    }
    else{
        return [];
    }
}

module.exports = modifySalary;