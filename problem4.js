// 4. Find the sum of all salaries.

const convertSalaryToNumber = require("./problem2.js");


function findSumOfSalary(employeeDetails){
    if(Array.isArray(employeeDetails)){
        let sumOfSalary = Number(0);
        employeeDetails = convertSalaryToNumber(employeeDetails);
        for(employee of employeeDetails){
            sumOfSalary += employee.salary;
        }
        return sumOfSalary;
    }
    else{
        return null;
    }
}

module.exports = findSumOfSalary;