// 5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

const convertSalaryToNumber = require("./problem2.js");

function groupSalaryByCountry(employeeDetails) {
    if (Array.isArray(employeeDetails)) {
        employeeDetails = convertSalaryToNumber(employeeDetails);
        let groupedByCountry = {};
        for (employee of employeeDetails) {
            if (!groupedByCountry[employee.location]) {
                groupedByCountry[employee.location] = employee.salary;
            }
            else {
                groupedByCountry[employee.location] += employee.salary;

            }
        }
        return groupedByCountry;
    }
    else {
        return [];
    }
}

module.exports = groupSalaryByCountry;