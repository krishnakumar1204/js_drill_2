// 6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).
const convertSalaryToNumber = require("./problem2.js");


function findAverageSalaryByCountry(employeeDetails) {
    if (Array.isArray(employeeDetails)) {
        employeeDetails = convertSalaryToNumber(employeeDetails);
        let averageSalaryByCountry = {};
        let employeeInACountry = {};
        for (let employee of employeeDetails) {
            if (!averageSalaryByCountry[employee.location] && !employeeInACountry[employee.location]) {
                averageSalaryByCountry[employee.location] = employee.salary;
                employeeInACountry[employee.location] = Number(1);
            }
            else {
                averageSalaryByCountry[employee.location] += employee.salary;
                employeeInACountry[employee.location]++;
            }
        }
        for (let key in averageSalaryByCountry) {
            if(averageSalaryByCountry.hasOwnProperty(key)){
                averageSalaryByCountry[key] /= employeeInACountry[key];
            }
        }
        return averageSalaryByCountry;
    }
    else {
        return [];
    }
}

module.exports = findAverageSalaryByCountry;