const employeeDetails = require("../data.js");
const findWebDevelopers = require("../problem1.js");

try {
    let webDevelopers = findWebDevelopers(employeeDetails);
    console.log(webDevelopers);
} catch (error) {
    console.log("Something went wrong");
}