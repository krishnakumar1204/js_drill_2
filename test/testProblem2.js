const employeeDetails = require("../data.js");
const convertSalaryToNumber = require("../problem2.js");

try {
    let convertedEmployeeDetails = convertSalaryToNumber(employeeDetails);
    console.log(convertedEmployeeDetails);
} catch (error) {
    console.log(error);
    console.log("Something went wrong");
}