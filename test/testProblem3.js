const employeeDetails = require("../data.js");
const modifySalary = require("../problem3.js");

try {
    let modifiedEmployeeDetails = modifySalary(employeeDetails);
    console.log(modifiedEmployeeDetails);
} catch (error) {
    console.log("Something went wrong");
}