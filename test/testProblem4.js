const employeeDetails = require("../data.js");
const findSumOfSalary = require("../problem4.js");

try {
    let sumOfSalary = findSumOfSalary(employeeDetails);
    console.log(Math.round(sumOfSalary*100)/100);
} catch (error) {
    console.log("Something went wrong");
}